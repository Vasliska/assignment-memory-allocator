
#define _DEFAULT_SOURCE

#include "test.h"

#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"


void debug(const char *fmt, ...);

static void* test_heap_init();
static void allocate_pages_after_block(struct block_header *last_block);
static struct block_header* get_block_from_contents(void * data);


void test1(){

    printf("start test))");

    void *data = _malloc(1000);
    if (data == NULL) {
        err("оу май тест 1 фэйл. _malloc return null");
    }

    debug_heap(stdout, first_block);

    if (first_block->is_free != false || first_block->capacity.bytes != 1000) {
        err("оу май тест 1 фэйл. is_free not correct");
    }
    printf("йоу тест 1 пройден");
    _free(data);    
}

void test2(){
    printf("Test 2\n");
    void *data1 = _malloc(1000);
    void *data2 = _malloc(1000);
    if (data1 == NULL || data2 == NULL) {
        err("оу май тест 2 фэйл. _malloc return null");
    }
    _free(data1);
    debug_heap(stdout, first_block);
    struct block_header *data1_block = get_block_from_contents(data1);
    struct block_header *data2_block = get_block_from_contents(data2);
    if (!data1_block->is_free || data2_block->is_free) {
        err("Test 2 fail");
    }
    debug("йоу тест 2 пройден\n");
    _free(data1);
    _free(data2);
}

void test3(){
    printf("Test 3\n");

    void *data1 = _malloc(INITIAL_HEAP_SIZE), *data2 = _malloc(INITIAL_HEAP_SIZE + 512), *data3 = _malloc(2048);
    if (data1 == NULL || data2 == NULL || data3 == NULL) {
        err("оу май тест 3 фэйл. _malloc return null\n");
    }
    _free(data3);
    _free(data2);

    debug_heap(stdout, first_block);

    struct block_header *data_block1 = get_block_by_allocated_data(data1), *data_block2 = get_block_by_allocated_data(data2);

    if ((uint8_t *)data_block1->contents + data_block1->capacity.bytes != (uint8_t*) data_block2){
        err("test 3 fail");
    }
    printf("test 3 ok\n");
    _free(data1);
    _free(data2);
    _free(data3);
}

void test4(){

    printf("Test 5\n");

    void *data1 = _malloc(10000);
    if (data1 == NULL) {
        err_red("оу май тест 3 фэйл. _malloc return null\n");
    }
    struct block_header *addr = first_block;
    while (addr->next != NULL) addr = addr->next;
    allocate_pages_after_block(addr);
    void *data2 = _malloc(100000);

    debug_heap(stdout, first_block);

    struct block_header *data2_block = get_block_by_allocated_data(data2);
    if (data2_block == addr) {
        err("Test 5 fail");
    }
    printf("OK. Test 5 passed\n\n");

    _free(data1);
    _free(data2);

}
